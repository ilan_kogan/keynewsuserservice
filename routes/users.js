require('dotenv').config();
//loading express module
const express = require('express');
const { secret } = require('config');
//initializing express Router to handdle the routes
const router = express.Router();
//loading the user scema model module
const User = require('../services/user.js');
//need when logging in a user
const jwt = require('jsonwebtoken');

// //to make authenticated routes
const passport = require("passport");
const formatUser = require('../helpers/userResponseFormat');

// register
router.post('/register', async function (req, res) {
    const { email, password } = req.body;
    try {
        await User.saveUser(email, password);
        res.status(200).json({
            message: 'Succeffuly signed up'
        })
    }
    catch (err) {
        res.status(500).json({ message: `Failed at registration, ${err}` });
    }
});

router.post('/login', async function (req, res) {
    const { email, password } = req.body;
    try {
        const user = await User.login(email, password);
        res.status(200).json({
            message: "Login Success",
            user: formatUser(user)
        });
    }
    catch (error) {
        res.status(500).json({ message: `${error}` });
    }
});

router.get('/keywords', passport.authenticate('jwt', { session: false }),
    async function (req, res) {
        try {
            const keywords = await User.getKeywords(req.user.Id);
            res.status(200).json({
                keywords
            });
        }
        catch (e) {
            res.status(500).json({ message: `${e}` });
        }
    });

router.post('/editKeyword', passport.authenticate('jwt', { session: false }),
    async function (req, res) {
        const { keywordId, value, language } = req.body;
        try {
            await User.editKeyword(keywordId, req.user.Id, value, language);
            res.status(200).json({
                message: "Succefully edited keyword"
            })
        }
        catch (e) {
            res.status(500).json({ message: `${e}` });
        }
    }
);

router.post('/createKeyword', passport.authenticate('jwt', { session: false }),
    async function (req, res) {
        const { value, language } = req.body;
        try {
            await User.createKeyword(req.user.Id, value, language);
            res.status(200).json({
                message: "Succefully created keyword"
            })
        }
        catch (e) {
            res.status(500).json({ message: `${e}` });
        }
    }
);

router.get('/linksCount', passport.authenticate('jwt', { session: false }),
    async function (req, res) {
        try {
            const count = await User.getLinksCount(req.user.Id);
            res.status(200).json({
                linksCount: count
            });
        }
        catch (e) {
            res.status(500).json({ message: `${e}` });
        }
    }
);

router.get('/autoSending', passport.authenticate('jwt', { session: false }),
    async function (req, res) {
        try {
            const autoSending = await User.getUserAutoSending(req.user.Id);
            res.status(200).json({
                autoSending
            });
        }
        catch (e) {
            res.status(500).json({ message: `${e}` });
        }
    }
);

router.post('/autoSending', passport.authenticate('jwt', { session: false }),
    async function (req, res) {
        const { value } = req.body;
        try {
            await User.changeUserAutoSending(req.user.Id, value);
            res.status(200).json({
                message: "Succefully change AutoSending status"
            })
        }
        catch (e) {
            res.status(500).json({ message: `${e}` });
        }
    }
);
//fetch autoSending keywords and links count
router.get('/allData', passport.authenticate('jwt', { session: false }),
    async function (req, res) {
        try {
            const autoSending = await User.getUserAutoSending(req.user.Id);
            const count = await User.getLinksCount(req.user.Id);
            const keywords = await User.getKeywords(req.user.Id);
            res.status(200).json({
                autoSending,
                linksCount: count,
                keywords
            });
        }
        catch (e) {
            res.status(500).json({ message: `${e}` });
        }
    }
);

router.delete('/keyword', passport.authenticate('jwt', { session: false }),
    async function (req, res) {
        const { keywordId } = req.body;
        try {
            await User.deleteKeyword(req.user.Id, keywordId);
            res.status(200).json({
                message: "Succefully deleted keyword"
            });
        }
        catch (e) {
            res.status(500).json({ message: `${e}` });
        }
    }
);

router.delete('/allkeywords', passport.authenticate('jwt', { session: false }),
    async function (req, res) {
        try {
            await User.deleteAllKeywords(req.user.Id);
            res.status(200).json({
                message: "Succefully deleted  all keywords"
            });
        }
        catch (e) {
            res.status(500).json({ message: `${e}` });
        }
    }
);

router.get('/articles', passport.authenticate('jwt', { session: false }),
    async function (req, res) {
        try {
            const { limit, offset } = req.query
            const { id } = req.body
            const articles = await User.getUserArticles(req.user.Id, limit, offset, id);
            res.status(200).json({
                articles
            });
        }
        catch (e) {
            res.status(500).json({ message: `${e}` });
        }
    }
);

module.exports = router;
