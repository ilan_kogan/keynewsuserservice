require('dotenv').config();
const jwt = require('jsonwebtoken');
const { secret } = require('config');
const { queryHandler } = require('../mysql/index');
var bcrypt = require('bcryptjs');
const userAccountsUtils = require("../utils/userAccountsUtils.js");

//validtors
const validator = require('validator')
const passwordValidator = require('password-validator')

//queries
const creaUserQuery = require('../mysql/queries/createUser');
const getUserByEmailQuery = require('../mysql/queries/getUserByEmail');
const getUserByIdQuery = require('../mysql/queries/getUserById');
const getUserKeywordsQuery = require('../mysql/queries/getUserKeywords');
const getKeywordQuery = require('../mysql/queries/getKeyword');
const getArticlesQuery = require('../mysql/queries/getUserArticles')

const editKeywordQuery = require('../mysql/queries/editKeyword');
const deleteKeywordQuery = require('../mysql/queries/deleteKeyword');
const deleteAllKeywordsQuery = require('../mysql/queries/deleteAllKeywords');

const createKeywordQuery = require('../mysql/queries/createKeyword');

const getLinksCountQuery = require('../mysql/queries/getUserLinkCount');

const editAutoSendingQuery = require('../mysql/queries/editAutoSending');

module.exports.getUserById = async function (userId) {
    const userResult = await queryHandler(getUserByIdQuery(userId));
    return userResult[0];
};

module.exports.login = async function (email, password) {
    const userResult = await queryHandler(getUserByEmailQuery(email));
    if (userResult.length === 0) throw new Error('User email does not exist');
    else {
        let match = await this.passwordCheck(password, userResult[0].Password);
        if (!match) throw new Error('wrong password');
        const token = jwt.sign({ userId: userResult[0].Id }, secret);
        let userToReturn = userResult[0];
        userToReturn.token = token;
        return userToReturn;
    }
};

module.exports.passwordCheck = function (plainPassword, hashPassword) {
    return new Promise((res, rej) => {
        bcrypt.compare(plainPassword, hashPassword, function (err, respond) {
            if (err) rej(err);
            res(respond);
        })
    })
};

module.exports.saveUser = async function (email, password) {
    const notValidResult = this.checkValidation(email, password)
    if (notValidResult)
        throw new Error(notValidResult)
    const userResult = await queryHandler(getUserByEmailQuery(email));
    if (userResult.length > 0) throw new Error('email is taken');
    const newUserToCreate = {
        email: email,
        password: await userAccountsUtils.hashPassword(password),
    };
    try {
        await queryHandler(creaUserQuery(newUserToCreate.email, newUserToCreate.password));
    }
    catch (error) {
        throw new Error(`error creating the user in the database ${error}`);
    }
};

module.exports.checkValidation = function (email, password) {
    var schema = new passwordValidator();
    schema
        .is().min(8)                                    // Minimum length 8
        .is().max(100)                                  // Maximum length 100
        .has().uppercase()                              // Must have uppercase letters
        .has().lowercase()                              // Must have lowercase letters
        .has().digits()                                 // Must have digits
        .has().not().spaces()                           // Should not have spaces
    if (validator.isEmail(email) && schema.validate(password)) {
        return false;
    }
    if (!validator.isEmail(email))
        return 'Please enter a valid Email';
    else
        return 'Password must have: uppercase,lowercase,digits and at least 8 characters';
};

module.exports.getKeywords = async function (userId) {
    const keywordsResult = await queryHandler(getUserKeywordsQuery(userId));
    keywordsResult.forEach(function (part, index) {
        this[index] = { id: this[index].Id, value: this[index].Value, language: this[index].Language };
    }, keywordsResult);
    return keywordsResult;
};

module.exports.editKeyword = async function (keywordId, userId, value, language) {
    const keywordResult = await queryHandler(getKeywordQuery(keywordId));
    if (keywordResult.length === 0) throw new Error('no such keyword');
    else {
        if (keywordResult[0].UserId !== userId) throw new Error('user doesnt allowed to edit this keyword');
        else {
            await queryHandler(editKeywordQuery(keywordId, value, language));
        }
    }
};


module.exports.createKeyword = async function (userId, value, language) {
    await queryHandler(createKeywordQuery(value, userId, language));
};

module.exports.getLinksCount = async function (userId) {
    return (await queryHandler(getLinksCountQuery(userId)))[0].linksCount;
};

module.exports.getUserAutoSending = async function (userId) {
    return (await queryHandler(getUserByIdQuery(userId)))[0].AutoSending;
};

module.exports.changeUserAutoSending = async function (userId, value) {
    return await queryHandler(editAutoSendingQuery(userId, value));
};

module.exports.deleteKeyword = async function (userId, keywordId) {
    const keywordResult = await queryHandler(getKeywordQuery(keywordId));
    if (keywordResult.length === 0) throw new Error('no such keyword');
    else {
        if (keywordResult[0].UserId !== userId) throw new Error('user doesnt allowed to delete this keyword');
        else {
            await queryHandler(deleteKeywordQuery(userId, keywordId));
        }
    }
};

module.exports.deleteAllKeywords = async function (userId) {
    const keywordsResult = await queryHandler(getUserKeywordsQuery(userId));
    if (keywordsResult.length === 0) throw new Error('There are no keywords in db');
    else
        await queryHandler(deleteAllKeywordsQuery(userId));
};

module.exports.getUserArticles = async function (userId, limit, offset, id) {
    const ArticlesResults = await queryHandler(getArticlesQuery(userId, limit, offset, id));
    ArticlesResults.forEach(function (part, index) {
        this[index] = {
            id: this[index].Id, link: this[index].Link, author: this[index].Author, title: this[index].Title, description: this[index].Description,
            imageLink: this[index].ImageLink, publishedAt: this[index].PublishedAt
        };
    }, ArticlesResults);
    return ArticlesResults;
};

// //getUser
// module.exports.getUserById = async function(userId){
//
// };
//
// module.exports.getUserKeyWords = async function(userId){
//     let userResponse = await getUserById(userId);
//     const isUserIdExists = userResponse.length > 0;
//     if (!isUserIdExists) throw new Error(`user id does not exist`);
//     else{
//         let userToReturn = userResponse[0];
//         const token = jwt.sign(userResponse[0], secret, {expiresIn:86400});
//         userToReturn.token = token;
//         return userToReturn;
//     }
// };
//
// //forgotPassword
// module.exports.recoverPassword = async function(email){
//     let userByEmailResponse = await getRegisteredUserByEmail(email);
//     const isUserExists = userByEmailResponse.length > 0;
//     if(isUserExists && userByEmailResponse[0].isLocal === true) {
//         let token = await userAccountsUtils.generateToken();
//         await ormApi.recoverPassword(token, email);
//         await userAccountsUtils.sendResetPasswordEmail(email, token, userByEmailResponse[0].id);
//     }
//     else{
//         throw new Error('there is no such user with that email')
//     }
//
// };
//
// module.exports.changePassword = async function(oldPassword, newPassword, userId){
//     const userResponse = await getRegisteredUserById(userId);
//     const isUserExists = userResponse.length > 0;
//     if(!isUserExists || userResponse[0].isLocal === false) throw new Error('User id doesn\'t exists');
//     else{
//         let match = await this.passwordCheck(oldPassword, userResponse[0].password);
//         if(!match) throw new Error('wrong old password');
//         const hashedNewPassword = await userAccountsUtils.hashPassword(newPassword);
//         try{
//             await axios.post(`${orm}${ormUsers}/changePassword`, {newPassword: hashedNewPassword, userId});
//         }
//         catch(error){
//             throw error;
//         }
//     }
// }
//
// module.exports.forgotChangePassword = async function(newPassword, token, userId){
//     const userResponse = await getRegisteredUserById(userId);
//     const isUserExists = userResponse.length > 0;
//     if(!isUserExists || userResponse[0].isLocal === false) throw new Error('User id doesn\'t exists');
//     else{
//         const user = userResponse[0];
//         if(user.resetPasswordToken !== token ||  Date.now() > new Date(user.resetPasswordExpiration)){
//             throw new Error('token is invalid or expired');
//             }
//         else{
//             hashedPassword = await userAccountsUtils.hashPassword(newPassword);
//             try{
//                 await ormApi.forgotChangePassword(token, hashedPassword, userId);
//             }
//             catch(error){
//                 throw new Error(`${error}`)
//             }
//         }
//     }
// };

