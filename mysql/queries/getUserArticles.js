module.exports = (userId, limit, offset, articleId) => {
    return (`
        SELECT *
        FROM sentarticles s
        where UserId=${userId} ${articleId === undefined ? '' : `And Id<=${articleId}`}
        ORDER BY Id DESC
        LIMIT ${offset},${limit};
    `)
};
