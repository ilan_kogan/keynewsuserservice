
module.exports.checkIfUserIdsValid = async function(userIdsToAdd, usersResponseFromGraphQl){
    let usersNotExist = [];
    let usersIdsResponse = usersResponseFromGraphQl.map((user) => parseInt(user.id));
    for(let userId of userIdsToAdd){
        if(!usersIdsResponse.includes(userId)) usersNotExist.push(userId);
    }
    return usersNotExist 
}