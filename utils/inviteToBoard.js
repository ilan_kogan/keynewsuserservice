require('dotenv').config();
const {emailAuth, emailAuthPassword} = require('config');
const nodemailer = require("nodemailer");

module.exports.sendEmailInviteToGroup = async function(email, token, groupId, inviteId){
    const smtpTransport = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: emailAuth,
            pass: emailAuthPassword
        }
    });
    var mailOptions = {
        to: email,
        from: 'goods4life11@gmail.com',
        subject: 'Invite link to board',
        text: `Join to your friends board at https://someClientUrl/inviteFromEmail?groupId=${groupId}&token=${token}&inviteId=${inviteId}`
    };
    smtpTransport.sendMail(mailOptions, function(err){
        if(err) throw err;
    })
};
